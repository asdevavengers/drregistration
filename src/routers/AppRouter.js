import React from 'react';
import { Router, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import MainPage from '../components/MainPage';
import PublicRoute from './PublicRoute';


export const history = createHistory();
class AppRouter extends React.Component {
  render() {
    return (
      <Router history={history} >
        <div>
          <Switch>
            <PublicRoute path="/" component={MainPage} exact={true}/>
            <PublicRoute  component={MainPage} />
          </Switch>
        </div>
      </Router>
    )
  }
};
export default AppRouter;
