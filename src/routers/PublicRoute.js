import React from 'react';
import { Route } from 'react-router-dom';
import Header from '../components/Header'
import Footer from '../components/Footer';

class PublicRoute extends React.Component {
    render() {
        const { component: Component, ...rest } = this.props;
        const props = this.props;
        return (
            <Route {...rest} component={(props) => (
                <div>
                    <Header {...props} />
                    <div className="container">
                        <div className="col-lg-12">
                            <Component {...props} />
                        </div>
                    </div>
                    <Footer />
                </div>
            )} />
        );
    };
}

export default PublicRoute;