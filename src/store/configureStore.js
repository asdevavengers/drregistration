import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import userReducer from '../reducers/users';
import thunk from 'redux-thunk';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose ;
export default () => {
    const store = createStore(combineReducers({
        users : userReducer,
      }),
      composeEnhancers(applyMiddleware(thunk))
    );
    return store;
}
//statusApt: AppointmentStatus