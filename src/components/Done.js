import React from 'react';


class Done extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        const _this = this;
        setTimeout(function() {
            _this.props.done();
         }, 5000);
    }
    render() {
        return (
            <div className='Done'>
                Thank you for giving your time ....
            </div>
        );
    }
}

export default Done;