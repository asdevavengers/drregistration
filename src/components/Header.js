import React from 'react';

class Header extends React.Component {
  render() {
    return (
      <header className="header container-full">
        <div style={{ width: '1075px', margin: '0 auto' }}>
          <h1>Patient Registration Form</h1>
        </div>
      </header>
    );
  }
};

export default Header;