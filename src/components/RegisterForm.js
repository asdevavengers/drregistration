import React from 'react';
import { Form, Input, Radio, DatePicker, Select, Button } from 'antd';
import moment from 'moment';
import jsPDF from 'jspdf';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uid: 0,
            insurance: [],
            user: {},
            name: '',
            dob: moment().format('DD-MM-YYYY'),
            ins_dob: moment().format('DD-MM-YYYY'),
            gender: 'male',
            address: '',
            city: '',
            state: '',
            zip: '',
            transportation: '',
            ssn: '',
            email: '',
            age: '',
            mr: '',
            phone: '',
            cell: '',
            marital_status: '',
            ins_gender: '',
            ins_lname: '',
            ins_fname: '',
            ins_address: '',
            ins_phone: '',
            ins_city: '',
            ins_state: '',
            ins_zip: '',
            patient_relation: '',
            employment_status: '',
            employer_name: '',
            dataSource: [],
            selectedCustomer: '',
            mr_number: ''
        }
    }
    dateChange = (date) => {
        let selectedDate = moment(date).format('DD-MM-YYYY');
        let age = moment().diff(date, 'years', false)
        this.setState({
            dob: selectedDate,
            age
        });
    }
    // changeActive = (e) => {
    //     let _this = this;
    //     let data = {};
    //     _this.props.loading(true);
    //     let len = this.state.insurance.length - 1;
    //     this.state.insurance.map((item, i) => {
    //         if (item.nid != e.target.value) {
    //             data = {
    //                 "_links": {
    //                     "type": {
    //                         "href": baseURL + "/rest/type/node/insurance"
    //                     }
    //                 },
    //                 "type": {
    //                     "target_id": "insurance"
    //                 },
    //                 "field_policy_status": {
    //                     "value": "insurance"
    //                 },
    //                 "field_policy_status": [{
    //                     "value": '0'
    //                 }]
    //             };
    //             _this.props.token().then(function (response) {
    //                 _this.props.updateContent(item.nid, data, response.data).then(function (response) {
    //                     if (len == i) {
    //                         _this.props.loading(false);
    //                         _this.props.getInsuranceByUid(_this.state.uid).then((res) => {
    //                             _this.setState({
    //                                 insurance: res.data
    //                             });
    //                         });
    //                     }
    //                 }).catch(function (error) {
    //                     _this.props.loading(false);
    //                     console.log(error);
    //                 });
    //             }).catch(function (error) {
    //                 _this.props.loading(false);
    //                 console.log(error);
    //             });
    //         }
    //     });
    //     let data1 = {
    //         "_links": {
    //             "type": {
    //                 "href": baseURL + "/rest/type/node/insurance"
    //             }
    //         },
    //         "type": {
    //             "target_id": "insurance"
    //         },
    //         "field_policy_status": {
    //             "value": "insurance"
    //         },
    //         "field_policy_status": [{
    //             "value": '1'
    //         }]
    //     };
    //     _this.props.token().then(function (response) {
    //         _this.props.updateContent(e.target.value, data1, response.data).then(function (response) {
    //             _this.props.loading(false);
    //             _this.props.getInsuranceByUid(_this.state.uid).then((res) => {
    //                 _this.setState({
    //                     insurance: res.data
    //                 });
    //             });
    //         }).catch(function (error) {
    //             _this.props.loading(false);
    //             console.log(error);
    //         });
    //     }).catch(function (error) {
    //         _this.props.loading(false);
    //         console.log(error);
    //     });
    // }
    // componentDidMount = () => {
    //     this.props.startGetUsers(undefined);
    // }
    // changeUser = (uid) => {
    //     let _this = this;
    //     this.props.getUserById(uid).then((response) => {
    //         let user = response.data[0];
    //         if( user.field_insured_dob ){
    //             _this.setState({
    //                 ins_dob:moment(user.field_insured_dob).format('DD-MM-YYYY')
    //             })
    //         }
    //         _this.setState({
    //             name: user.field_first_name + " " + user.field_last_name,
    //             dob: moment(user.field_birthday).format('DD-MM-YYYY'),
    //             gender: user.field_gender,
    //             age: moment().diff(user.field_birthday, 'years', false),
    //             address: user.field_address,
    //             city: user.field_city,
    //             state: user.field_state,
    //             zip: user.field_zip_code,
    //             transportation: user.field_transportation,
    //             ssn: user.field_s_s_n,
    //             email: user.mail,
    //             mr: user.field_code,
    //             phone: user.field_phone,
    //             cell: user.field_cell,
    //             marital_status: user.field_marital_status,
    //             ins_gender: user.field_insured_gender,
    //             ins_lname: user.field_insured_last_name,
    //             ins_fname: user.field_insured_first_name,
    //             ins_address: user.field_insured_home_address,
    //             ins_phone: user.field_insured_phone,
    //             ins_city: user.field_insured_city,
    //             ins_state: user.field_insured_state,
    //             ins_zip: user.field_insured_zip,
    //             patient_relation: user.field_patient_relation,
    //             employment_status: user.field_employment_status,
    //             employer_name: user.field_employer_name,
    //             mr_number : user.field_mrnumber,
    //             uid,
    //             user
    //         });
    //     });
    //     this.props.getInsuranceByUid(uid).then((res) => {
    //         _this.setState({
    //             insurance: res.data
    //         });
    //     });
    // }
    // onInputChange = (value) => {
    //     this.props.startGetUsers(value);
    // }
    // componentDidUpdate = (prevProps) => {
    //     if (this.props.users !== prevProps.users) {
    //         let dataSource = [];
    //         this.props.users.map(
    //             (row) => {
    //                 if (!!this.state.uid || this.state.uid == row.uid) {
    //                     this.setState({
    //                         selectedCustomer: {
    //                             label: row.field_first_name + " " + row.field_last_name,
    //                             value: row.uid
    //                         },

    //                     });
    //                 }
    //                 dataSource.push({
    //                     label: row.field_first_name + " " + row.field_last_name,
    //                     value: row.uid
    //                 })
    //             }
    //         )
    //         this.setState({ dataSource });
    //     }
    // }
    // handleSubmit = () => {
    //     let insuranceData = {
    //         ins_gender: this.state.ins_gender,
    //         employment_status: this.state.employment_status,
    //         employer_name: this.state.employer_name,
    //         ins_fname: this.state.ins_fname,
    //         ins_lname: this.state.ins_lname,
    //         ins_address: this.state.ins_address,
    //         ins_city: this.state.ins_city,
    //         ins_state: this.state.ins_state,
    //         ins_zip: this.state.ins_zip,
    //         ins_phone: this.state.ins_phone,
    //         ins_dob: this.state.ins_dob,
    //         patient_relation: this.state.patient_relation,
    //         mr_number: this.state.mr_number
    //     };
    //     fetch('http://localhost:3001/api/saveinsure', {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify(insuranceData)
    //     }).then(res => res.json()).then(res => {
    //             //console.log("Line 237",res)
    //         }).catch(error => console.log(error));
    //     let name = this.state.name.split(' ');
        
    //     const data = {
    //         "_links": baseLinks,

    //         "field_first_name": [{
    //             "value": name[0]
    //         }],
    //         "field_last_name": [{
    //             "value": (name[1]) ? name[1] : ''
    //         }],
    //         "field_gender": [{
    //             "value": this.state.gender
    //         }],
    //         "field_address": [{
    //             "value": this.state.address
    //         }],
    //         "field_city": [{
    //             "value": this.state.city
    //         }],
    //         "field_state": [{
    //             "value": this.state.state
    //         }],
    //         "field_zip_code": [{
    //             "value": this.state.zip
    //         }],
    //         "field_birthday": [{
    //             "value": moment(this.state.dob, 'DD-MM-YYYY').format('YYYY-MM-DD')
    //         }],
    //         "field_cell": [{
    //             "value": this.state.cell
    //         }],
    //         "field_phone": [{
    //             "value": this.state.phone
    //         }],
    //         "field_s_s_n": [{
    //             "value": this.state.ssn
    //         }],
    //         "field_marital_status": [{
    //             "value": this.state.marital_status
    //         }],
    //         "field_transportation": [{
    //             "value": this.state.transportation
    //         }],
    //         "field_patient_relation": [{
    //             "value": this.state.patient_relation
    //         }],
    //         "field_employer_name": [{
    //             "value": this.state.employer_name
    //         }],
    //         "field_employment_status": [{
    //             "value": this.state.employment_status
    //         }],
    //         "field_insured_first_name": [{
    //             "value": this.state.ins_fname
    //         }],
    //         "field_insured_last_name": [{
    //             "value": this.state.ins_lname
    //         }],
    //         "field_insured_phone": [{
    //             "value": this.state.ins_phone
    //         }],
    //         "field_insured_gender": [{
    //             "value": this.state.ins_gender
    //         }],
    //         "field_insured_home_address": [{
    //             "value": this.state.ins_address
    //         }],
    //         "field_insured_city": [{
    //             "value": this.state.ins_city
    //         }],
    //         "field_insured_dob": [{
    //             "value": moment(this.state.ins_dob, 'DD-MM-YYYY').format('YYYY-MM-DD')
    //         }],
    //         "field_insured_state": [{
    //             "value": this.state.ins_state
    //         }],
    //         "field_insured_zip": [{
    //             "value": this.state.ins_zip
    //         }],
    //     };
    //     let _this = this;
    //     _this.props.loading(true);
    //     _this.props.token().then(function (response) {
    //         _this.props.updateUser(_this.state.uid, data, response.data).then(function (response) {
    //             //console.log("Line 324",response);
    //             _this.props.loading(false);
    //         }).catch(function (error) {
    //             console.log(error);
    //         });
    //     }).catch(function (error) {
    //         console.log(error);
    //     });
    // }
    render() {
        return (
            <div className="container">
                <div className="main-page-bg profile">
                    <div className='selection-buttons' >
                        <div className='title-patient-info' style={{ display: 'inline-block' }}>
                            <h1 className='title' style={{ margin: '18px 0px 0px 0px' }}>Patient Information</h1>
                        </div>
                    </div>
                    <div className="profile-form-container">
                        <form id='profile-form' >
                            <div className='patient-info'>
                                <div className='patient-detail'>
                                    <div className='patient-info-left'>
                                        <FormItem label='Patient Name'>
                                            <Input
                                                value={this.state.name}
                                                onChange={(e) => {
                                                    this.setState({ name: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Gender'>
                                            <RadioGroup onChange={(e) => {
                                                this.setState({ gender: e.target.value });
                                            }} value={this.state.gender}>
                                                <Radio value='male'>Male</Radio>
                                                <Radio value='female'>Female</Radio>

                                            </RadioGroup>
                                        </FormItem>
                                        <FormItem label='Street Address'>
                                            <Input
                                                value={this.state.address}
                                                onChange={(e) => {
                                                    this.setState({ address: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <div className='patient-address'>
                                            <FormItem label='City' className='city'>
                                                <Input
                                                    value={this.state.city}
                                                    onChange={(e) => {
                                                        this.setState({ city: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                            <FormItem label='State' className='state'>
                                                <Input
                                                    value={this.state.state}
                                                    onChange={(e) => {
                                                        this.setState({ state: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                            <FormItem label='Zip Code' className='zipcode'>
                                                <Input
                                                    value={this.state.zip}
                                                    onChange={(e) => {
                                                        this.setState({ zip: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                        </div>
                                        <FormItem label='Email Address'>
                                            <Input
                                                value={this.state.email}
                                                disabled={true}
                                                onChange={(e) => {
                                                    this.setState({ email: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='S.S.N'>
                                            <Input
                                                value={this.state.ssn}
                                                onChange={(e) => {
                                                    this.setState({ ssn: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Transportation'>
                                            <Input
                                                value={this.state.transportation}
                                                onChange={(e) => {
                                                    this.setState({ transportation: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                    </div>
                                    <div className='patient-info-right'>
                                        <div className='patient-info-dob'>
                                            <FormItem label='D.O.B' className='dob'>
                                                <DatePicker
                                                    value={moment(this.state.dob, 'DD-MM-YYYY')}
                                                    format='MM-DD-YYYY'
                                                    onChange={this.dateChange}
                                                />
                                            </FormItem>
                                            <FormItem label='Age'>
                                                <Input
                                                    value={this.state.age}
                                                    disabled={true}
                                                />
                                            </FormItem>
                                        </div>
                                        <FormItem label='Mr #'>
                                            <Input
                                                value={this.state.mr}
                                                disabled={true}
                                                onChange={(e) => {
                                                    this.setState({ mr: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Phone'>
                                            <Input
                                                value={this.state.phone}
                                                onChange={(e) => {
                                                    this.setState({ phone: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Cell'>
                                            <Input
                                                value={this.state.cell}
                                                onChange={(e) => {
                                                    this.setState({ cell: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Marital Status' className='marital-status'>
                                            <Select
                                                style={{ width: 200 }}
                                                onChange={(marital_status) => {
                                                    this.setState({ marital_status });
                                                }}
                                                value={this.state.marital_status}
                                            >
                                                <Option value="Single">Single</Option>
                                                <Option value="Married">Married</Option>
                                                <Option value="Other">Other</Option>

                                            </Select>
                                        </FormItem>
                                    </div>
                                </div>
                            </div>
                            <div className='insurance-details'>
                                <div className='insurance-det'>
                                    <h1 className='title'>Insurance Information</h1>
                                    <div className='insurance-information'>
                                        <FormItem label='Insured Gender'>
                                            <Select
                                                style={{ width: 200 }}
                                                onChange={(ins_gender) => {
                                                    this.setState({ ins_gender });
                                                }}
                                                value={this.state.ins_gender}
                                            >
                                                <Option value="f">Female</Option>
                                                <Option value="m">Male</Option>

                                            </Select>
                                        </FormItem>
                                        <FormItem label='Employment Status'>
                                            <Select
                                                style={{ width: 200 }}
                                                onChange={(employment_status) => {
                                                    this.setState({ employment_status });
                                                }}
                                                value={this.state.employment_status}
                                            >
                                                <Option value="employed">Employed</Option>
                                                <Option value="ft">Student FT</Option>
                                                <Option value="pt">Student PT</Option>
                                                <Option value="unemployed">Unemployed</Option>
                                            </Select>
                                        </FormItem>
                                        <FormItem label='Employer Name'>
                                            <Input
                                                value={this.state.employer_name}
                                                onChange={(e) => {
                                                    this.setState({ employer_name: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                    </div>
                                    <div className='insured-contact'>
                                        <FormItem label='First Name'>
                                            <Input
                                                value={this.state.ins_fname}
                                                onChange={(e) => {
                                                    this.setState({ ins_fname: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Last Name'>
                                            <Input
                                                value={this.state.ins_lname}
                                                onChange={(e) => {
                                                    this.setState({ ins_lname: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <FormItem label='Home Address'>
                                            <Input
                                                value={this.state.ins_address}
                                                onChange={(e) => {
                                                    this.setState({ ins_address: e.target.value });
                                                }}
                                            />
                                        </FormItem>
                                        <div className='patient-address'>
                                            <FormItem label='City' className='city'>
                                                <Input
                                                    value={this.state.ins_city}
                                                    onChange={(e) => {
                                                        this.setState({ ins_city: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                            <FormItem label='State' className='state'>
                                                <Input
                                                    value={this.state.ins_state}
                                                    onChange={(e) => {
                                                        this.setState({ ins_state: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                            <FormItem label='Zip' className='zipcode'>
                                                <Input
                                                    value={this.state.ins_zip}
                                                    onChange={(e) => {
                                                        this.setState({ ins_zip: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                        </div>
                                        <div className='patient-phone-insured'>
                                            <FormItem label='Phone #' className='phone'>
                                                <Input
                                                    value={this.state.ins_phone}
                                                    onChange={(e) => {
                                                        this.setState({ ins_phone: e.target.value });
                                                    }}
                                                />
                                            </FormItem>
                                            <FormItem label='Insured DOB' className='insured'>
                                                <DatePicker
                                                    value={moment(this.state.ins_dob, 'DD-MM-YYYY')}
                                                    format='MM-DD-YYYY'
                                                    onChange={(date) => {
                                                        let selectedDate = moment(date).format('DD-MM-YYYY');
                                                        this.setState({
                                                            ins_dob: selectedDate,
                                                        });
                                                    }}
                                                />
                                            </FormItem>
                                        </div>
                                        <FormItem label='Patient Relation' className='patient-relation'>
                                            <Select
                                                style={{ width: 200 }}
                                                onChange={(patient_relation) => {
                                                    this.setState({ patient_relation });
                                                }}
                                                value={this.state.patient_relation}
                                            >
                                                <Option value="child">Child</Option>
                                                <Option value="self">Self</Option>
                                                <Option value="other">Other</Option>
                                                <Option value="spouse">Spouse</Option>
                                            </Select>
                                        </FormItem>
                                    </div>
                                </div>
                                <Button className='save-changes' type="primary" onClick={this.handleSubmit}>Save Changes</Button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        )
    }
};


export default Profile;