import React from 'react';
import { Form, Icon, Input, Button, Checkbox, Select, Steps, DatePicker } from 'antd';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { sendMail } from '../actions/send_mail';
import { getToken, registorUser, updateUser } from '../actions/users';
import { getUserIdByemail } from '../actions/loadUser';
// import { Alert } from 'react-bootstrap';
import { baseLinks } from '../config/config';
import validator from 'validator';
import moment from 'moment';
import axios from 'axios';
// import { getMaxListeners } from 'cluster';
const { TextArea } = Input;
const Option = Select.Option;
const FormItem = Form.Item;

class NormalForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fname: '',
            lname: '',
            email: '',
            email1: '',
            address: '',
            address1: '',
            cell: '',
            night_phone: '',
            day_phone: '',
            dob: moment('01-01-1960', 'MM-DD-YYYY'),
            gender: "male",
            profe: '',
            city: '',
            state: '',
            zip: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.compareEmails = this.compareEmails.bind(this);
    }
    compareEmails = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('email')) {
            callback('Two Emails that you enter is inconsistent!');
        } else {
            callback();
        }
    }
    saveToCsv = () =>  {

        let data = {
            fname : this.state.fname,
            lname : this.state.lname,
            dob : this.state.dob.format('YYYY-MM-DD'),
            age : 23,
            email : this.state.email,
            address: this.state.address,
            address1: this.state.address1,
            cell: this.state.cell,
            night_phone: this.state.night_phone,
            day_phone: this.state.day_phone,
            gender: this.state.gender,
            profe: this.state.profe,
            city: this.state.city,
            state: this.state.state,
            zip: this.state.zip
          }
          //localhost:3001/api/save
        fetch('http://localhost:3001/api/save', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .then(res => {
                this.setState({
                        fname: '',
                        lname: '',
                        email: '',
                        email1: '',
                        address: '',
                        address1: '',
                        cell: '',
                        night_phone: '',
                        day_phone: '',
                        dob: moment('01-01-1960', 'MM-DD-YYYY'),
                        gender: "male",
                        profe: 'Other',
                        city: '',
                        state: '',
                        zip: ''
                })
                console.log(res)
            })
            .catch(error => console.log(error))
    }
    handleSubmit(e) {
        e.preventDefault();
        const _this = this;
        let mail = this.state.email;
        let pass = this.state.fname + "@123456";
        let data = {
            // "_links": baseLinks,
            "field_first_name": this.state.fname,
            "field_last_name": this.state.lname,
            "field_gender": this.state.gender,
            "field_address": this.state.address,
            "field_birthday": this.state.dob.format('YYYY-MM-DD'),
            "field_cell": this.state.cell,
            "field_night_ph": this.state.night_phone,
            "field_day_ph": this.state.day_phone,
            "field_access_level": "customer",
            "field_profe": this.state.profe,
            "field_city": this.state.city,
            "field_state": this.state.state,
            "field_zip_code": this.state.zip,
            "name": mail,
            "mail": mail,
            "pass": pass
        };
        this.props.form.validateFields((err, values) => {
            if (!err) {
                
                _this.props.getToken().then(function (response) {
                    _this.props.registorUser(data, response.data).then(function (response) {
                        let uid = response.data.user.uid[0].value;
                        let name = response.data.user.field_first_name[0].value;
                        _this.saveToCsv();
                        _this.props.next();
                        _this.props.set(uid, name);
                        _this.props.sendMail({
                            uid: response.data.user.uid[0].value,
                            token: pass,
                            mail
                        });
                    }).catch(function (error) { console.log(error); });
                }).catch(function (error) { console.log(error); });
            }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className='register-form'>
                <form id='form-register' onSubmit={this.handleSubmit}>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="First Name:" >
                                        {getFieldDecorator('fname', {
                                            rules: [{ required: true, message: 'Please input your Name!' }],
                                        })(
                                            <Input
                                                name='fname'
                                                onChange={event => {
                                                    this.setState({
                                                        fname: event.target.value
                                                    });
                                                }}
                                            />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Last Name:" >
                                        {getFieldDecorator('lname', {
                                        })(
                                            <Input
                                                name='lname'
                                                onChange={event => {
                                                    this.setState({
                                                        lname: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>

                                    <FormItem label="Birth Date:" >
                                        {getFieldDecorator('dob', {
                                            initialValue: this.state.dob,
                                            rules: [{ required: true, message: 'Please input your Date of Birth!' }],
                                        })(
                                            <DatePicker
                                                name='dob'
                                                format='MM-DD-YYYY'
                                                onChange={(date, dateString) => {
                                                    this.setState({
                                                        dob: date
                                                    });
                                                }}
                                            />
                                        )}
                                    </FormItem>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12'>


                            <div className='col-sm-3 select-list-gen'>
                                <div className='form-element'>
                                    <FormItem label="Gender:" >
                                        {getFieldDecorator('gender', {
                                            rules: [{ required: true, message: 'Please input your gender!' }],
                                        })(
                                            <Select name='gender'
                                                onChange={(value) => {
                                                    this.setState({ gender: value })
                                                }}>
                                                <Option value='male'>Male</Option>
                                                <Option value='female'>Female</Option>
                                            </Select>
                                        )}
                                    </FormItem>
                                </div>
                            </div>


                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Address:" >
                                        {getFieldDecorator('address', {

                                        })(
                                            <Input
                                                name='address1'
                                                onChange={event => {
                                                    this.setState({
                                                        address: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="City:" >
                                        {getFieldDecorator('city', {
                                        })(
                                            <Input
                                                name='city'
                                                onChange={event => {
                                                    this.setState({
                                                        city: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12'>

                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="State:" >
                                        {getFieldDecorator('state', {
                                        })(
                                            <Input
                                                name='state'
                                                onChange={event => {
                                                    this.setState({
                                                        state: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Zip:" >
                                        {getFieldDecorator('zip', {
                                        })(
                                            <Input
                                                name='zip'
                                                onChange={event => {
                                                    this.setState({
                                                        zip: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Email Address:" >
                                        {getFieldDecorator('email', {
                                            rules: [{
                                                type: 'email', message: 'The input is not valid E-mail!',
                                            }, {
                                                required: true, message: 'Please input your E-mail!',
                                            }],
                                        })(
                                            <Input
                                                name='email'
                                                onChange={event => {
                                                    this.setState({
                                                        email: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Confirm Email:" >
                                        {getFieldDecorator('confirm_email', {
                                            rules: [{
                                                type: 'email', message: 'The input is not valid E-mail!',
                                            }, {
                                                required: true, message: 'Please input your E-mail!',
                                            }, {
                                                validator: this.compareEmails,
                                            }],
                                        })(
                                            <Input
                                                name='confirm_email'
                                                onChange={event => {
                                                    this.setState({
                                                        email1: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Profession:" >
                                        <Select defaultValue="Other" style={{ width: "100%" }}
                                            onChange={value => {
                                                this.setState({
                                                    profe: value
                                                });
                                            }}
                                        >
                                            <Option value="Agriculture">Agriculture</Option>
                                            <Option value="Architecture">Architecture</Option>
                                            <Option value="Biological and Biomedical Sciences">Biological and Biomedical Sciences</Option>
                                            <Option value="Business">Business</Option>
                                            <Option value="Communications and Journalism">Communications and Journalism</Option>
                                            <Option value="Computer Sciences">Computer Sciences</Option>
                                            <Option value="Culinary Arts and Personal Services">Culinary Arts and Personal Services</Option>
                                            <Option value="Education">Education</Option>
                                            <Option value="Engineering">Engineering</Option>
                                            <Option value="Legal">Legal</Option>
                                            <Option value="Liberal Arts and Humanities">Liberal Arts and Humanities</Option>
                                            <Option value="Mechanic and Repair Technologies">Mechanic and Repair Technologies</Option>
                                            <Option value="Medical and Health Professions">Medical and Health Professions</Option>
                                            <Option value="Other">Other</Option>
                                        </Select>
                                    </FormItem>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Mobile Phone:" >
                                        {getFieldDecorator('cell_phone', {

                                            rules: [{ required: true, message: 'Please input your Mobile Number!' }],
                                        })(
                                            <Input
                                                name='cell_phone'
                                                onChange={event => {
                                                    this.setState({
                                                        cell: event.target.value
                                                    });
                                                }}
                                            />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Night Phone:" >
                                        {getFieldDecorator('night_phone', {

                                        })(
                                            <Input
                                                name='night_phone'
                                                onChange={event => {
                                                    this.setState({
                                                        night_phone: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                            <div className='col-sm-3'>
                                <div className='form-element'>
                                    <FormItem label="Day Phone:" >
                                        {getFieldDecorator('day_phone', {

                                        })(
                                            <Input
                                                name='day_phone'
                                                onChange={event => {
                                                    this.setState({
                                                        day_phone: event.target.value
                                                    });
                                                }} />
                                        )}
                                    </FormItem>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <Button type="primary" htmlType="submit" className="register-form-button">  Next >></Button>
                        </div>
                    </div>

                </form>
            </div>
        );
    }
}
const MainPage = Form.create()(NormalForm);
const mapDispatchToProps = (dispatch, props) => ({
    getToken: () => dispatch(getToken()),
    getUserIdByemail: (mail) => dispatch(getUserIdByemail(mail)),
    registorUser: (user) => dispatch(registorUser(user)),
    sendMail: (data) => dispatch(sendMail(data)),
    updateUser: (uid, user, token) => dispatch(updateUser(uid, user, token))
});
export default connect(undefined, mapDispatchToProps)(MainPage);