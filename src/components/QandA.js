import React from 'react';
import { Form, Icon, Input, Button, Checkbox, Select, Radio, Steps, DatePicker } from 'antd';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getToken } from '../actions/users';
import { addContent } from '../actions/add_content';
// import createHistory from 'history/createBrowserHistory';
// import { Alert } from 'react-bootstrap';
import { baseURL } from '../config/config';
import validator from 'validator';
import moment from 'moment';

const { TextArea } = Input;
const Option = Select.Option;
const RadioGroup = Radio.Group;
const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;

const options = [
    { label: 'Any exfoliating scrubs', value: 'scrubs' },
    { label: 'Any hydroxy acid product', value: 'acid_product' },
    { label: 'Vitamin A', value: 'vitamin_a' },
    { label: 'Glycolic acid', value: 'glycolic_acid' },
    { label: 'Lactic acid', value: 'lactic_acid' },
];
class QandAForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            q1_option:0,
            q1_explain:'',
            q2_option:0,
            q3_option:0,
            q3_explain:'',
            q4_option:0,
            q4_explain:'',
            q5_option:0,
            q5_explain:'',
            products:[],
            q6_option:0,
            q6_explain:'',
            q8_option:0,
            q9_option:0,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    onChange(products){
      this.setState({ products });
    }
    handleSubmit(e) {
        e.preventDefault();
        const data = {
            "_links": {
                "type": {
                    "href": baseURL + "/rest/type/node/q_and_a"
                }
            },
            "type": {
                "target_id": "q_and_a"
            },
            "title": {
                "value": this.props.name + "'s Q and A "
            },
            "field_health_concerns_option": [{
                "value": this.state.q1_option
            }],
            "field_q1_health_concerns": [{
                "value": this.state.q1_explain
            }],
            "field_are_you_pregnant": [{
                "value": this.state.q2_option
            }],
            "field_allergies_or_sensitivities": [{
                "value": this.state.q3_option
            }],
            "field_q_2_allergies_sensitivitie": [{
                "value": this.state.q3_explain
            }],
            "field_special_concern": [{
                "value": this.state.q4_explain
            }],
            "field_special_concern_options": [{
                "value": this.state.q4_option
            }],
            "field_resurfacing_treatments": [{
                "value": this.state.q5_explain
            }],
            "field_resurfacing_treatments_opt": [{
                "value": this.state.q5_option
            }],
            "field_skin_products": [{
                "value": this.state.q6_explain
            }],
            "field_skin_products_option": [{
                "value": this.state.q6_option
            }],
             "field_using_any_products": this.state.products.map(ele => {
                return { "value": ele }
            }),
            "field_do_you_have_rosacea": [{
                "value": this.state.q8_option
            }],
            "field_do_you_consider_your_skin_": [{
                "value": this.state.q9_option
            }],
            "field_patient": [{
                "target_id": this.props.uid
            }],
        };
        const _this = this;
        _this.props.getToken().then(function (response) {
            _this.props.addContent(data, response.data).then(function (response) {
                _this.props.next();
            }).catch(function (error) {
                console.log(error);
            });
        }).catch(function (error) {
            console.log(error);
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className='qanda-form'>
                <form id='form-qanda' onSubmit={this.handleSubmit}>
                    <div className='row'>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="1.	Do you have any health concerns that your therapist should be aware of ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q1_option: e.target.value,
                                      });
                                }} value={this.state.q1_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                            <FormItem label="If yes, please explain" >
                                <Input
                                    onChange={event => {
                                        this.setState({
                                            q1_explain: event.target.value
                                        });
                                    }}
                                />
                            </FormItem>
                        </div>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="2.	Are you pregnant ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q2_option: e.target.value,
                                      });
                                }} value={this.state.q2_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                        </div>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="3.	Do you have any allergies or sensitivities to products ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q3_option: e.target.value,
                                      });
                                }} value={this.state.q3_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                            <FormItem label="If yes, please explain" >
                                    <Input
                                        onChange={event => {
                                            this.setState({
                                                q3_explain: event.target.value
                                            });
                                        }}
                                    />
                            </FormItem>
                        </div>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="4.	Do you have any special concerns in regards to your face and/or body ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q4_option: e.target.value,
                                      });
                                }} value={this.state.q4_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                            <FormItem label="If yes, please explain" >
                                    <Input
                                        onChange={event => {
                                            this.setState({
                                                q4_explain: event.target.value
                                            });
                                        }}
                                    />
                            </FormItem>
                        </div>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="5.	Have you ever had chemical peels, microdermabrasion, or any resurfacing treatments ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q5_option: e.target.value,
                                      });
                                }} value={this.state.q5_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                            <FormItem label="If yes, please explain" >
                                    <Input
                                        onChange={event => {
                                            this.setState({
                                                q5_explain: event.target.value
                                            });
                                        }}
                                    />
                            </FormItem>
                        </div>
                        
                        <div className='col-sm-12 q-container'>
                            <FormItem label="6.	Do you use Accutane, Adapalene, Renova, Retin A, or any other prescribed skin products ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q6_option: e.target.value,
                                      });
                                }} value={this.state.q6_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                            <FormItem label="If yes, please explain" >
                                    <Input
                                        onChange={event => {
                                            this.setState({
                                                q6_explain: event.target.value
                                            });
                                        }}
                                    />
                            </FormItem>
                        </div>
                      
                        <div className='col-sm-12 q-container'>
                            <FormItem label="7.	Are you using any products that contain the following" >
                                <CheckboxGroup options={options} onChange={this.onChange} />
                            </FormItem>
                        </div>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="8.	Do you have rosacea ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q8_option: e.target.value,
                                      });
                                }} value={this.state.q8_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                </RadioGroup>
                            </FormItem>
                        </div>
                        <div className='col-sm-12 q-container'>
                            <FormItem label="9.	Do you consider your skin “sensitive” ?" >
                                <RadioGroup onChange={(e)=>{
                                     this.setState({
                                        q9_option: e.target.value,
                                      });
                                }} value={this.state.q9_option}>
                                    <Radio value={0}>No</Radio>
                                    <Radio value={1}>Yes</Radio>
                                    <Radio value={2}>Somewhat</Radio>
                                </RadioGroup>
                            </FormItem>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-sm-12'>
                            <Button type="primary" htmlType="submit" className="form-qanda-button">Process</Button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
const QandA = Form.create()(QandAForm);
const mapDispatchToProps = (dispatch, props) => ({
    getToken: () => dispatch(getToken()),
    addContent: (data, token) => dispatch(addContent(data, token)),
});
export default connect(undefined, mapDispatchToProps)(QandA);