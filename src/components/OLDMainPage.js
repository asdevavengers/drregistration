import React from 'react';
import { Form, Input, Radio, DatePicker, Select, Button, Modal } from 'antd';
import moment from 'moment';
import { connect } from 'react-redux';
import { sendMail } from '../actions/send_mail';
import { getTerms } from '../actions/getTerms';
import { addContent } from '../actions/add_content';
import { getToken, registorUser, updateUser } from '../actions/users';
import { getUserIdByemail } from '../actions/loadUser';
import { baseURL } from '../config/config';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      patientName: '',
      patientGender: 'male',
      patientAddress: '',
      patientCity: '',
      patientState: '',
      patientZip: '',
      patientEmail: '',
      patientSsn: '',
      patientTransportation: '',
      patientDob: '',
      patientAge: '',
      patientPhone: '',
      patientCell: '',
      patientMarital_status: '',

      ins_gender: '',
      ins_employment_status: '',
      ins_employer_name: '',
      ins_reffer: '',
      ins_lname: '',
      ins_fname: '',
      ins_address: '',
      ins_city: '',
      ins_state: '',
      ins_zip: '',
      ins_phone: '',
      ins_dob: '',
      patient_relation: '',

      location: '',
      category: '',
      category_pay: '',
      emergency_name: '',
      emergency_number: '',
      notes: '',

      table_insurance: '',
      table_fname: '',
      table_lname: '',
      table_pid: '',
      table_copay: '',
      table_deductable: '',

      table_insurance_array: []
    }
  }
  dateChange = (date) => {
    let selectedDate = moment(date).format('DD-MM-YYYY');
    let age = moment().diff(date, 'years', false)
    this.setState({
      patientDob: selectedDate,
      patientAge: age
    });
  }
  saveToJSON = (data) => {
    fetch('http://localhost:3001/api/save', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .catch(error => console.log(error))
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let data = {
          patientName: values.patientName,
          patientGender: this.state.patientGender,
          patientAddress: this.state.patientAddress,
          patientCity: this.state.patientCity,
          patientState: this.state.patientState,
          patientZip: this.state.patientZip,
          patientEmail: this.state.patientEmail,
          patientSsn: this.state.patientSsn,
          patientTransportation: this.state.patientTransportation,
          patientDob: this.state.patientDob,
          patientAge: this.state.patientAge,
          patientPhone: this.state.patientPhone,
          patientCell: this.state.patientCell,
          patientMarital_status: this.state.patientMarital_status,

          ins_gender: this.state.ins_gender,
          ins_employment_status: this.state.ins_employment_status,
          ins_employer_name: this.state.ins_employer_name,
          ins_reffer: this.state.ins_reffer,
          ins_lname: this.state.ins_lname,
          ins_fname: this.state.ins_fname,
          ins_address: this.state.ins_address,
          ins_city: this.state.ins_city,
          ins_state: this.state.ins_state,
          ins_zip: this.state.ins_zip,
          ins_phone: this.state.ins_phone,
          ins_dob: this.state.ins_dob,
          patient_relation: this.state.patient_relation,

          location: this.state.location,
          category: this.state.category,
          category_pay: this.state.category_pay,
          emergency_name: this.state.emergency_name,
          emergency_number: this.state.emergency_number,
          notes: this.state.notes
        }

        this.props.getToken().then((response) => {
          this.props.registorUser(data, response.data).then((response) => {
            this.saveToJSON(data);
            this.submitTableData(response.data.user.uid[0].value);
            this.countDown();
            this.clearForm();
            this.props.form.resetFields();
          }).catch((error) => { console.log(error); });
        }).catch((error) => { console.log(error); });
      }
    })
  }
  countDown = () => {
    let secondsToGo = 5;
    const modal = Modal.success({
      title: <h3 style={{ margin: 0 }}>Thank you for registering with us!</h3>,
      centered: true
    });
    const timer = setInterval(() => {
      secondsToGo -= 1;
      modal.update({
        // content: `This modal will be destroyed after ${secondsToGo} second.`,
      });
    }, 1000);
    setTimeout(() => {
      clearInterval(timer);
      modal.destroy();
    }, secondsToGo * 1000);
  }
  clearForm = () => {
    this.setState({
      patientName: '',
      patientGender: 'male',
      patientAddress: '',
      patientCity: '',
      patientState: '',
      patientZip: '',
      patientEmail: '',
      patientSsn: '',
      patientTransportation: '',
      patientDob: '',
      patientAge: '',
      patientPhone: '',
      patientCell: '',
      patientMarital_status: '',

      ins_gender: '',
      ins_employment_status: '',
      ins_employer_name: '',
      ins_reffer: '',
      ins_lname: '',
      ins_fname: '',
      ins_address: '',
      ins_city: '',
      ins_state: '',
      ins_zip: '',
      ins_phone: '',
      ins_dob: '',
      patient_relation: '',

      location: '',
      category: '',
      category_pay: '',
      emergency_name: '',
      emergency_number: '',
      notes: '',

      table_insurance: '',
      table_fname: '',
      table_lname: '',
      table_pid: '',
      table_copay: '',
      table_deductable: '',

      table_insurance_array: []
    })
  }
  componentDidMount = () => {
    this.props.getTerms(148).then(res => {
      this.setState({ table_insurance_array: res.data })
    })
  }
  submitTableData = (uid) => {
    let title = this.state.table_insurance_array.filter((item) => {
      return (item.tid == this.state.table_insurance);
    })
    const data = {
      "_links": {
        "type": {
          "href": baseURL + "/rest/type/node/insurance"
        }
      },
      "type": {
        "target_id": "insurance"
      },
      "title": {
        "value": title[0].name
      },
      "field_policy_status": [{
        "value": '1'
      }],
      "field_patient_id": [{
        "value": this.state.table_pid
      }],
      "field_insurance": [{
        "target_id": this.state.table_insurance
      }],
      "field_co_pay": [{
        "value": this.state.table_copay
      }],
      "field_deductable": [{
        "value": this.state.table_deductable
      }],
      "field_user_policy": [{
        "target_id": uid
      }],
    };
    // console.log(data);
    const _this = this;
    _this.props.getToken().then(function (response) {
      _this.props.addContent(data, response.data).then(function (response) {
        // _this.countDown();
      }).catch(function (error) {
        console.log(error);
      });
    }).catch(function (error) {
      console.log(error);
    });
  }
  render() {
    var nameArray = this.state.patientName.split(" ");
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="" style={{ marginBottom: 70 }}>
        <div className="main-page-bg profile">
          <div className='selection-buttons' >
            <div className='title-patient-info' style={{ display: 'inline-block' }}>
              <h3 className='title' style={{ margin: '18px 0px 0px 0px' }}>Patient Information</h3>
            </div>
          </div>
          <div className="profile-form-container">
            <form id='profile-form' >
              <div className='full-width-area'>
                <div className='mainpage-left-side' style={{ width: '80%', float: "left" }}>
                  <div className='patient-info'>
                    <div className='patient-detail'>
                      <div className='patient-info-left'>
                        <FormItem label='Patient Name' className='patient-name'>
                          {getFieldDecorator('patientName', {
                            rules: [{ required: true, message: 'Please enter name' }],
                          })(
                            <Input
                              onChange={(e) => {
                                this.setState({ patientName: e.target.value });
                              }}
                            />
                          )}
                        </FormItem>
                        <FormItem label='Gender'>
                          <RadioGroup onChange={(e) => {
                            this.setState({ patientGender: e.target.value });
                          }} value={this.state.patientGender}>
                            <Radio value='male'>Male</Radio>
                            <Radio value='female'>Female</Radio>
                          </RadioGroup>
                        </FormItem>
                        <FormItem label='Street Address' className='street-address'>
                          <Input
                            value={this.state.patientAddress}
                            onChange={(e) => {
                              this.setState({ patientAddress: e.target.value });
                            }}
                          />
                        </FormItem>
                        <div className='patient-address'>
                          <FormItem label='City' className='city'>
                            <Input
                              value={this.state.patientCity}
                              onChange={(e) => {
                                this.setState({ patientCity: e.target.value });
                              }}
                            />
                          </FormItem>
                          <FormItem label='State' className='state'>
                            <Input
                              value={this.state.patientState}
                              onChange={(e) => {
                                this.setState({ patientState: e.target.value });
                              }}
                            />
                          </FormItem>
                          <FormItem label='Zip Code' className='zipcode'>
                            <Input
                              value={this.state.patientZip}
                              onChange={(e) => {
                                this.setState({ patientZip: e.target.value });
                              }}
                            />
                          </FormItem>
                        </div>
                        <FormItem label='Email Address' className='emailaddress'>
                          <Input
                            value={this.state.patientEmail}
                            onChange={(e) => {
                              this.setState({ patientEmail: e.target.value });
                            }}
                          />
                        </FormItem>
                        <FormItem label='S.S.N'>
                          <Input
                            value={this.state.patientSsn}
                            onChange={(e) => {
                              this.setState({ patientSsn: e.target.value });
                            }}
                          />
                        </FormItem>
                        <FormItem label='Transportation' className='transportation'>
                          <Input
                            value={this.state.patientTransportation}
                            onChange={(e) => {
                              this.setState({ patientTransportation: e.target.value });
                            }}
                          />
                        </FormItem>
                      </div>
                      <div className='patient-info-right'>
                        <div className='patient-info-dob'>
                          <FormItem label="D.O.B" className='dob' >
                            {getFieldDecorator('patientDob', {
                              initialValue: this.state.patientDob
                            })(
                              <DatePicker
                                name='patientDob'
                                format='MM/DD/YYYY'
                                onChange={this.dateChange}
                              />
                            )}
                          </FormItem>
                          <FormItem label='Age' className='age'>
                            <Input
                              value={this.state.patientAge}
                              disabled={true}
                            />
                          </FormItem>
                        </div>
                        <FormItem label='MR Number' className='mrnumber'>
                          <Input disabled />
                        </FormItem>
                        <FormItem label='Home Phone' className='homephone'>
                          <Input
                            value={this.state.patientPhone}
                            onChange={(e) => {
                              this.setState({ patientPhone: e.target.value });
                            }}
                          />
                        </FormItem>
                        <FormItem label='Cell Phone' className='cellphone'>
                          <Input
                            value={this.state.patientCell}
                            onChange={(e) => {
                              this.setState({ patientCell: e.target.value });
                            }}
                          />
                        </FormItem>
                        <FormItem label='Marital Status' className='marital-status'>
                          <Select
                            style={{ width: 200 }}
                            onChange={(patientMarital_status) => {
                              this.setState({ patientMarital_status });
                            }}
                            value={this.state.patientMarital_status}
                          >
                            <Option value="Single">Single</Option>
                            <Option value="Married">Married</Option>
                            <Option value="Other">Other</Option>
                          </Select>
                        </FormItem>
                      </div>
                    </div>
                  </div>

                  <div className='insurance-details'>
                    <div className='insurance-det'>
                      <div className='insurance-information'>
                        <h3 className='title'>Insurance Information</h3>
                        <FormItem label='Insured Gender'>
                          <Select
                            // style={{ width: 200 }}
                            onChange={(ins_gender) => {
                              this.setState({ ins_gender });
                            }}
                            value={this.state.ins_gender}
                          >
                            <Option value="f">Female</Option>
                            <Option value="m">Male</Option>
                          </Select>
                        </FormItem>
                        <FormItem label='Employment Status'>
                          <Select
                            // style={{ width: 200 }}
                            onChange={(ins_employment_status) => {
                              this.setState({ ins_employment_status });
                            }}
                            value={this.state.ins_employment_status}
                          >
                            <Option value="employed">Employed</Option>
                            <Option value="ft">Student FT</Option>
                            <Option value="pt">Student PT</Option>
                            <Option value="unemployed">Unemployed</Option>
                          </Select>
                        </FormItem>
                        <FormItem label='Employer Name'>
                          <Input
                            value={this.state.ins_employer_name}
                            onChange={(e) => {
                              this.setState({ ins_employer_name: e.target.value });
                            }}
                          />
                        </FormItem>
                        <FormItem label='Reffered by'>
                          <Input
                            value={this.state.ins_reffer}
                            onChange={(e) => {
                              this.setState({ ins_reffer: e.target.value });
                            }}
                          />
                        </FormItem>
                      </div>
                      <div className='insured-contact'>
                        <h3 className='title'>Insured Contact Details</h3>
                        <div className='f-l-name'>
                          <FormItem label='Last Name' className='lname'>
                            <Input
                              value={this.state.ins_lname}
                              onChange={(e) => {
                                this.setState({ ins_lname: e.target.value });
                              }}
                            />
                          </FormItem>
                          <FormItem label='First Name' className='fname'>
                            <Input
                              value={this.state.ins_fname}
                              onChange={(e) => {
                                this.setState({ ins_fname: e.target.value });
                              }}
                            />
                          </FormItem>
                        </div>
                        <FormItem label='Home Address' className='home-address'>
                          <Input
                            value={this.state.ins_address}
                            onChange={(e) => {
                              this.setState({ ins_address: e.target.value });
                            }}
                          />
                        </FormItem>
                        <div className='patient-address'>
                          <FormItem label='City' className='city'>
                            <Input
                              value={this.state.ins_city}
                              onChange={(e) => {
                                this.setState({ ins_city: e.target.value });
                              }}
                            />
                          </FormItem>
                          <FormItem label='State' className='state'>
                            <Input
                              value={this.state.ins_state}
                              onChange={(e) => {
                                this.setState({ ins_state: e.target.value });
                              }}
                            />
                          </FormItem>
                          <FormItem label='Zip' className='zipcode'>
                            <Input
                              value={this.state.ins_zip}
                              onChange={(e) => {
                                this.setState({ ins_zip: e.target.value });
                              }}
                            />
                          </FormItem>
                        </div>
                        <div className='patient-phone-insured'>
                          <FormItem label='Phone #' className='phone'>
                            <Input
                              value={this.state.ins_phone}
                              onChange={(e) => {
                                this.setState({ ins_phone: e.target.value });
                              }}
                            />
                          </FormItem>
                          <FormItem label="Insured DOB" className='insured' >
                            {getFieldDecorator('ins_dob', {
                              initialValue: this.state.ins_dob
                            })(
                              <DatePicker
                                name='ins_dob'
                                format='MM/DD/YYYY'
                                onChange={(date, dateString) => {
                                  this.setState({
                                    ins_dob: date
                                  });
                                }}
                              />
                            )}
                          </FormItem>
                        </div>
                        <FormItem label='Patient Relation' className='patient-relation'>
                          <Select
                            style={{ width: 200 }}
                            onChange={(patient_relation) => {
                              this.setState({ patient_relation });
                            }}
                            value={this.state.patient_relation}
                          >
                            <Option value="child">Child</Option>
                            <Option value="self">Self</Option>
                            <Option value="other">Other</Option>
                            <Option value="spouse">Spouse</Option>
                          </Select>
                        </FormItem>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='mainpage-right-side' style={{ width: '20%', float: 'right', padding: 15 }}>
                  <div>
                    <div className='emergancy-contact'>
                      <h4>Emergancy Contact</h4>
                      <FormItem label='Name' className='emergencyname'>
                        <Input
                          value={this.state.emergency_name}
                          onChange={(e) => {
                            this.setState({ emergency_name: e.target.value });
                          }}
                        />
                      </FormItem>
                      <FormItem label='Number' className='emergencynumber'>
                        <Input
                          value={this.state.emergency_number}
                          onChange={(e) => {
                            this.setState({ emergency_number: e.target.value });
                          }}
                        />
                      </FormItem>
                    </div>
                  </div>
                  <div classNam='selectlists'>
                    <FormItem label='Location' className='location'>
                      <Select
                        placeholder="location"
                        onChange={(location) => {
                          this.setState({ location });
                        }}
                      >
                        <Option value="brooklyn">Brooklyn</Option>
                        <Option value="long_island">Long Island</Option>
                      </Select>
                    </FormItem>
                    <FormItem label='Category' className='category'>
                      <Select
                        placeholder="Category"
                        onChange={(category) => {
                          this.setState({ category });
                        }}
                      >
                        <Option value="botox">Botox</Option>
                        <Option value="cancer">Cancer</Option>
                        <Option value="cosmetic">Cosmetic</Option>
                        <Option value="functional_madicine">Functional Madicine</Option>
                        <Option value="general">General</Option>
                        <Option value="glutathione">Glutathione</Option>
                        <Option value="hrt">HRT</Option>
                        <Option value="iv">IV</Option>
                        <Option value="restylene">Restylene</Option>
                        <Option value="thyroid">Thyroid</Option>
                      </Select>
                    </FormItem>
                    <FormItem label='Category Pay' className='category-pay'>
                      <Select
                        style={{ width: '100%' }}
                        placeholder="Insurance"
                        onChange={(category_pay) => {
                          this.setState({ category_pay });
                        }}
                      >
                        <Option value="general">General</Option>
                        <Option value="cash">Cash</Option>
                      </Select>
                    </FormItem>
                    <FormItem label='Note' className='emergencynumber'>
                      <TextArea rows={4}
                        value={this.state.notes}
                        onChange={(e) => {
                          this.setState({ notes: e.target.value });
                        }}
                      />
                    </FormItem>
                  </div>


                </div>
              </div>

              <div className='table-form-data' style={{ overflow: 'hidden', width: '100%' }}>
                <table>
                  <tr>
                    <th>Insurance</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Patient ID</th>
                    <th>Co-pay</th>
                    <th>MR Number</th>
                    <th>Deductable</th>
                  </tr>
                  <tr>
                    <td>
                      <Select
                        placeholder="Insurance"
                        style={{ width: 150 }}
                        onChange={(table_insurance) => {
                          this.setState({ table_insurance });
                        }}
                      >
                        {
                          this.state.table_insurance_array ?
                            this.state.table_insurance_array.map(item => {
                              return <Option value={item.tid}>{item.name}</Option>
                            })
                            : null
                        }
                      </Select>
                    </td>
                    <td>
                      <Input
                        value={nameArray ? nameArray[0] : ''}
                        onChange={(e) => {
                          this.setState({ table_fname: e.target.value });
                        }}
                      />
                    </td>
                    <td>
                      <Input
                        value={nameArray ? nameArray[1] : ''}
                        onChange={(e) => {
                          this.setState({ table_lname: e.target.value });
                        }}
                      />
                    </td>
                    <td>
                      <Input
                        value={this.state.table_pid}
                        onChange={(e) => {
                          this.setState({ table_pid: e.target.value });
                        }}
                      />
                    </td>
                    <td>
                      <Input
                        value={this.state.table_copay}
                        onChange={(e) => {
                          this.setState({ table_copay: e.target.value });
                        }}
                      />
                    </td>
                    <td>
                      <Input
                        disabled
                      />
                    </td>
                    <td>
                      <Input
                        value={this.state.table_deductable}
                        onChange={(e) => {
                          this.setState({ table_deductable: e.target.value });
                        }}
                      />
                    </td>
                  </tr>
                </table>
              </div>

              <div className='submitbutton' style={{ overflow: 'hidden', width: '100%' }}>
                <Button size='large' className='save-changes' type="primary" onClick={this.handleSubmit}>Save</Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
};

const validateProfile = Form.create()(Profile);
const mapDispatchToProps = (dispatch, props) => ({
  getToken: () => dispatch(getToken()),
  getUserIdByemail: (mail) => dispatch(getUserIdByemail(mail)),
  registorUser: (user) => dispatch(registorUser(user)),
  sendMail: (data) => dispatch(sendMail(data)),
  getTerms: (vid) => dispatch(getTerms(vid)),
  updateUser: (uid, user, token) => dispatch(updateUser(uid, user, token)),
  addContent: (data, token) => dispatch(addContent(data, token)),
});
export default connect(undefined, mapDispatchToProps)(validateProfile);