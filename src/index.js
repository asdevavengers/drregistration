import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import './app.global.css';

ReactDOM.render(
  <Provider store={configureStore()}>
    <AppRouter />
  </Provider>
  , document.getElementById('root'));