const defaultUsersState = {
    users : []
};
export default (state = defaultUsersState, action) => {
    switch (action.type) {  
        case "GET_USERS":
            return {
                users : action.users
            };
        default:
            return state;
    }
}