import axios from 'axios';
import { baseURL } from '../config/config';

export const sendMail = (data) => {
    return (dispatch) => {
       return axios({
            method: 'POST',
            url: baseURL + '/api/post_mail.json',
            headers: {
                'Content-Type': 'application/hal+json',
            },
            data: JSON.stringify(data)
        })
    };
};