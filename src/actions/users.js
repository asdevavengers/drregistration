import axios from 'axios';
import { baseURL, baseAuth, baseHeaders } from '../config/config';

export const startGetUsers = (name = undefined) => {
    let requestUrl = "/api/user-listing?_format=hal_json";
    if (!!name) {
        requestUrl += `&name=${name}`;
    }
    return (dispatch) => {
        return axios({
            method: 'get',
            url: requestUrl,
            baseURL: baseURL,
            auth: baseAuth,
        });
    };
};
export const GetUserById = (uid) => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `/user/${uid}?_format=hal_json`,
            baseURL: baseURL,
            headers: baseHeaders,
            auth: baseAuth,
        });
    };
};
export const getUserIdByemail = (mail) => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `/api/check-user?_format=hal_json&mail=${mail}`,
            baseURL: baseURL,
            headers: baseHeaders,
            auth: baseAuth,
        });
    };
};
export const getToken = () => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: baseURL + '/rest/session/token',
            headers: {
                'Content-Type': 'application/hal+json',
            },
        })
    };
};
export const registorUser = (user,token) => {
    return (dispatch) => {
       return axios({
            method: 'post',
            // url: baseURL + '/user/register?_format=hal_json',
            url: baseURL + '/user_registoration/post.json',
            headers: {
                'Content-Type': 'application/hal+json',
                'X-CSRF-Token': token
            },
            data: JSON.stringify(user)
        })
    };
};
export const updateUser = (uid,user,token) => {
    return (dispatch) => {
       return axios({
            method: 'PATCH',
            url: baseURL + `/user/${uid}?_format=hal_json`,
            headers: {
                'Content-Type': 'application/hal+json',
                'X-CSRF-Token': token
            },
            auth: baseAuth,
            data: JSON.stringify(user)
        })
    };
};