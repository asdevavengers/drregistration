import axios from 'axios';
import { baseURL, baseAuth, baseHeaders } from '../config/config';


export const getUserIdByemail = (mail) => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `/api/check-user?_format=hal_json&mail=${mail}`,
            baseURL: baseURL,
            headers: baseHeaders,
            auth: baseAuth,
        });
    };
};