import axios from 'axios';
import { baseURL,baseAuth } from '../config/config';

export const getArchiveNumber = () => {
    let requestUrl = `/api/createarchive.json`;
    return (dispatch) => {
        return axios({
            method: 'GET',
            url: requestUrl,
            baseURL: baseURL
        });
    };
};