import axios from 'axios';
import { baseURL, baseAuth, baseHeaders } from '../config/config';

export const deleteContent = (nid,token) => {
    return (dispatch) => {
       return axios({
            method: 'DELETE',
            url: baseURL + `/node/${nid}?_format=hal_json`,
            headers: {
                'Content-Type': 'application/hal+json',
                'X-CSRF-Token': token
            },
            auth: baseAuth,
        })
    };
};