import axios from 'axios';
import { baseURL, baseAuth, baseHeaders } from '../config/config';

export const addContent = (data,token) => {

    return (dispatch) => {
       return axios({
            method: 'POST',
            url: baseURL + '/node?_format=hal_json',
            headers: {
                'Content-Type': 'application/hal+json',
                'X-CSRF-Token': token
            },
            auth:baseAuth,
            data: JSON.stringify(data)
        })
    };
};