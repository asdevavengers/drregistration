import axios from 'axios';
import { baseURL, baseAuth, baseHeaders } from '../config/config';


export const getTerms = (vid) => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: `api/ins-terms?_format=hal_json`,
            baseURL: baseURL,
            headers: baseHeaders,
            auth: baseAuth,
        });
    };
};