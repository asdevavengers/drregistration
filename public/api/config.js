module.exports = {
  server: {
    host: 'localhost',
    port: 3044,
  },
  database: {
    mainCollection: 'users',
  },
};
