const userService = require('../services/user.service');
var fs = require('fs');
var child_process = require('child_process');

module.exports = (app, db) => {
  const userServiceInstance = userService(db);

  app.get('/api/user', (req, res) => userServiceInstance.get().then(obj => res.send(obj.data)));

  app.get('/api/user/:id', (req, res) => userServiceInstance.getById(req.params.id).then(obj => res.send(obj)));

  app.put('/api/user/:id', (req, res) => userServiceInstance.put(req.params.id, req.body).then(obj => res.send(obj)));

  app.post('/api/user', (req, res) => userServiceInstance.post(req.body).then(obj => res.send(obj)));

  app.delete('/api/user/:id', (req, res) => userServiceInstance.delete(req.params.id).then(obj => res.send(obj)));

  app.get('/api', (req, res, next) => {
    res.send('response')
  })

  app.post('/api/save', (req, res, next) => {
    const records = {
      // fname: req.body.fname,
      // lname: req.body.lname,
      // email: req.body.email,
      // address: req.body.address,
      // address1: req.body.address1,
      // age: req.body.age,
      // cell: req.body.cell,
      // city: req.body.city,
      // day_phone: req.body.day_phone,
      // dob: req.body.dob,
      // gender: req.body.gender,
      // night_phone: req.body.night_phone,
      // profe: req.body.profe,
      // state: req.body.state,
      // zip: req.body.zip,
      patientName: req.body.patientName,
      patientGender: req.body.patientGender,
      patientAddress: req.body.patientAddress,
      patientCity: req.body.patientCity,
      patientState: req.body.patientState,
      patientZip: req.body.patientZip,
      patientEmail: req.body.patientEmail,
      patientSsn: req.body.patientSsn,
      patientTransportation: req.body.patientTransportation,
      patientDob: req.body.patientDob,
      patientAge: req.body.patientAge,
      patientMR: req.body.patientMR,
      patientPhone: req.body.patientPhone,
      patientCell: req.body.patientCell,
      patientMarital_status: req.body.patientMarital_status,
      ins_gender: req.body.ins_gender,
      ins_employment_status: req.body.ins_employment_status,
      ins_employer_name: req.body.ins_employer_name,
      ins_reffer: req.body.ins_reffer,
      ins_lname: req.body.ins_lname,
      ins_fname: req.body.ins_fname,
      ins_address: req.body.ins_address,
      ins_city: req.body.ins_city,
      ins_state: req.body.ins_state,
      ins_zip: req.body.ins_zip,
      ins_phone: req.body.ins_phone,
      ins_dob: req.body.ins_dob,
      patient_relation: req.body.patient_relation
    };

    var json = JSON.stringify(records);
    fs.writeFile('C:/Temp/register/file.json', json, (err) => {
      if (!err) {
        child_process.exec('C:/Temp/register/runMSAccess.bat', function (error, stdout, stderr) {
          res.status(200).json({
            message: 'success'
          })
        });
      } else {
        res.status(500).json({
          message: 'error'
        })
      }
    });
  });
};