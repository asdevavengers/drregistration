const userService = require('../services/user.service');
var fs = require('fs');
var child_process = require('child_process');
var moment = require('moment');

module.exports = (app, db) => {
  const userServiceInstance = userService(db);

  app.get('/api/user', (req, res) => userServiceInstance.get().then(obj => res.send(obj.data)));

  app.get('/api/user/:id', (req, res) => userServiceInstance.getById(req.params.id).then(obj => res.send(obj)));

  app.put('/api/user/:id', (req, res) => userServiceInstance.put(req.params.id, req.body).then(obj => res.send(obj)));

  app.post('/api/user', (req, res) => userServiceInstance.post(req.body).then(obj => res.send(obj)));

  app.delete('/api/user/:id', (req, res) => userServiceInstance.delete(req.params.id).then(obj => res.send(obj)));

  app.get('/api', (req, res, next) => {
    res.send('response')
  })

  app.post('/api/save', (req, res, next) => {
    const records = {
      patientName: req.body.patientName,
      patientGender: req.body.patientGender,
      patientAddress: req.body.patientAddress,
      patientCity: req.body.patientCity,
      patientState: req.body.patientState,
      patientZip: req.body.patientZip,
      patientEmail: req.body.patientEmail,
      patientSsn: req.body.patientSsn,
      patientTransportation: req.body.patientTransportation,
      patientDob: req.body.patientDob,
      patientAge: req.body.patientAge,
      patientMR: req.body.patientMR,
      patientPhone: req.body.patientPhone,
      patientCell: req.body.patientCell,
      patientMarital_status: req.body.patientMarital_status,
      ins_gender: req.body.ins_gender,
      ins_employment_status: req.body.ins_employment_status,
      ins_employer_name: req.body.ins_employer_name,
      ins_reffer: req.body.ins_reffer,
      ins_lname: req.body.ins_lname,
      ins_fname: req.body.ins_fname,
      ins_address: req.body.ins_address,
      ins_city: req.body.ins_city,
      ins_state: req.body.ins_state,
      ins_zip: req.body.ins_zip,
      ins_phone: req.body.ins_phone,
      ins_dob: req.body.ins_dob,
      patient_relation: req.body.patient_relation,
      location: req.body.location,
      category: req.body.category,
      category_pay: req.body.category_pay,
      emergency_name: req.body.emergency_name,
      emergency_number: req.body.emergency_number,
      notes: req.body.notes,
      mr: req.body.mr,
      ins: req.body.ins,
      patient_id: req.body.patient_id,
      copay: req.body.copay,
      deductable: req.body.deductable
    };
    //create pdf start 
    //var pdf = require('html-pdf');

    var html = '<div style="padding:20px"><p style="font-weight:bold;text-align:center;border-bottom:1px solid;padding-left: 115px">Sergey Kalitenko, MD <span style="float: right;">Date: '+ moment().format('MM/DD/YYYY') +'</span></p><div style="text-align:center;font-weight:bold;">2158 Ocean Ave <br>Brooklyn, NY, 11229 <br>718-382-9200 <br>www.kalitenko.com</div><br>'+
    '<div style="text-align:center;"><span style="border-bottom:1px solid;font-weight:bold;">ACKNOWLEDGEMENT OF RECEIPT OF HIPAA NOTICE OF PRIVACY PRACTICES</span></div><br>'+
    '<p>By signing below, I acknowledge that I have been provided a copy of this Notice of Privacy Practices and have therefore been advised of how health information about me may be used and disclosed by the hospital and the facilities listed at the beginning of this Notice, and how I may obtain access to and control this information. I also acknowledge and understand that I may request copies of separate written explanations of special privacy protections that apply to HIV related information and mental health information.<br></p>'+
    '<br><br><span style="border-top:1px solid;">Signature of Patient or Personal Representative </span><br><br>'+
    '<br><br><span style="border-top:1px solid;">Print Name of Patient or Personal Representative </span><br>'+
    '<br><br><span>Date: ___/___/___</span><br>'+
    '<br><br><span style="border-top:1px solid;">Description of Personal Representatives Authority</span><br>'+
    '<br><div style="text-align:center;"><span style="text-align:center;font-weight:bold;">(For internal use - where sigoature above cannot be obtained)</span><div>'+
    '<p style="text-align:left;">Except in emergency treatment circumstances, the Health Insurance Portability and Accountability Act of 1996 ("HIPAA") requires that we make a good faith effort to obtain written acknowledgement of the patient s receipt of the Notice of Privacy Practices on the first date after April 14, 2003 we provide treatment, products or services to the patient (including at the time of admission, at a first visit to a hospital department, or any first service contact with the patient). We must make a goods faith effort to obtain written acknowledgement when reasonably practicable following an emergency treatment situation. If such acknowledgement cannot be obtained, we must document our good faith effort to obtain the acknowledgement and why it was obtained. </p>'+
    '<br><br><p style="text-align:left;">Describe good faith effort to obtain written acknowledgement (including your name and the date)</p>'+
    '<p style="text-align:left;">1. _________________________________________________________</p>'+
    '<p style="text-align:left;">Name:______________________________________ Date:___/___/___</p>'+
    '<p style="text-align:left;">2. _________________________________________________________</p>'+
    '<p style="text-align:left;">Name:______________________________________ Date:___/___/___</p>'+
    '<p style="text-align:left;">3. _________________________________________________________</p>'+
    '<p style="text-align:left;">Name:______________________________________ Date:___/___/___</p>'+
    '<br><div style="text-align:center;"><p>THE ORIGINAL OF THIS FORM MUST BE PLACED IN THE MEDICAL RECORD</p></div>'+
    '<br><br><br><br><br><br><br><br><br><div style="text-align:center;font-weight:bold;">SERGEY A. KALITENKO, PHYSICIAN, P.C.<br>'+ 
    'INTERNAL MEDICINE<br>'+
    '2158 OCEAN AVENUE, BROOKLYN, NY 11229<br>'+ 
    'TEL.: (718) 382 – 9200 FAX: (718) 382-9201<br>'+ 
    'Medical Consent / Authorization Form<br></div>'+
    '<br><br><br><div style="text-align:left;"><br><span>Date:___/___/___</span></div><br><br>'+
    '<div style="text-align:left"><span style="text-align:left;font-weight:bold;">Consept for Treatment</span> I authorize the above named Doctor(s), to perform the treatment procedure(s) described below. I have been informed of the reasons for the treatment procedure(s), along with the expected benefits, risks, possible alternative methods of treatment, and possible involved in the following:<br>'+
    '______________________________________________________________________________________________<br>'+
    '______________________________________________________________________________________________<br>'+
    '______________________________________________________________________________________________<br>'+
    '<br><span style="text-align:left;">The treatment procedure(s) were explained to me in detail and all my questions were fully answered.<br> Understanding this. I authorize the above named doctor(s) to performed such examinations, treatments,<br> laboratory test, and to administer such medication as in his or her opinion, are necessary or advisable for me (or </span>'+
    '_______________________________________________________) Name of patient if minor</div><br><br>'+
    '<p style="text-align:left;"><br>I also cenify that no guarantee or assurance has been made as to the results that may be obtained.</p>'+
    '<p style="text-align:left;"><span style="font-weight:bold">Release of Medical Record</span> In order to ensure proper follow-up and continuity of care. I agree that a copy of my medical record may be released to my physician, a designated referral physician, and/or the provider, if any, who referred me here. </p>'+
    '<p style="text-align:left;"><span style="font-weight:bold">Insurance Authorization</span>I request that payment of authorized benefits be made to the above-named Doctor(s) on my behalf, for any services provided to me. I authorize any holder of medical and other information about me to release to Medicare and to its agents, any insurance company, any other third party payer, state medical assistant agency, or any other governmental or private payer responsible for paying such benefits, any information needed to determine these benefits for related services. I agree to pay for all charges not covered by a third party payer. I authorize a copy of this authorization to be used in place of the original.</p>'+
    '<br><br><p style="text-align:left;">signed:_________________________________________         Date:___/___/___</p>'+
    '<p style="text-align:left;margin-left:50px;">Patient or person authorize to consent for patient</p>'+
    '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>'+
    '<br><br><br><p style="text-align:left;"><span style="font-weight:bold"><u>Assignments of Insurance Benefits</u></span><br> I hereby authorize direct payment of surgical/medical benefits to Dr. Kalitenko. For services rendered by him/her in person or under his/her supervision. I understand that I am financially responsible for any balance not covered by my insurance.</p>'+
    '<br><p style="text-align:left;"><span style="font-weight:bold"><u>Authorization to Release Information</u></span><br>I hereby authorize Dr. Kalitenko to release any medical information that may be necessary for either medical care or in processing applications for financial benefits.</p>'+
    '<br><p style="text-align:left;"><span style="font-weight:bold"><u>Signature on File</u></span><br>I request that payment of authorized Medicare benefits be made either to me or on my behalf to _______________________________________  for services furnished to me by provider. I authorize any holder of medical information about me to release to the Health Care Financing Administration and its agents any information acceded to determine these benefits payable for related services.</p>'+
    '<br><p style="text-align:left;"><span style="font-weight:bold"><u>Internet Options</u></span><br>I give my permission to Dr. Kalitenko Medical Office and Medical Spa to send promotional materials to my mailing address, e-mail address and to my phone through text messaging. I agree not to use e-mail and office e-mail address for emergency or medical problems and questions.</p>'+
    '<br><br><p style="text-align:center;font-weight:bold;">A photocopy of these assignments shall be valid as the original. </p>'+
    '<p style="text-align:left;">Patient Name (Print)______________________________ Date____________</p><br>'+
    '<p style="text-align:left;">Patient Signature   __________________________________________</p><br>'+
    '<p style="text-align:left;">Providers Signature __________________________________________</p><br>'+
    '<p style="text-align:left;">Parent/Guardian (Print)______________________________ Signature____________</p><br>'+
    '<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>'+
    '<br><br><br><br><br><br><br><table style="width:100%"><tr>'+
      '<h2>PATIENT REGISTRATION</h2><br>'+
      '<td>'+
        '<span style="font-weight:bold;">Patient Name:</span> '+req.body.patientName+'<br>'+ 
        '<span style="font-weight:bold;">Patient Gender:</span> '+req.body.patientGender+'<br>'+
        '<span style="font-weight:bold;">Patient Address:</span> '+req.body.patientAddress+', '+req.body.patientCity+' '+req.body.patientState+' '+req.body.patientZip+'<br>'+
        '<span style="font-weight:bold;">Patient Email:</span> '+req.body.patientEmail+'<br>'+
        '<span style="font-weight:bold;">S.S.N:</span> '+req.body.patientSsn+'<br>'+
        '<span style="font-weight:bold;">Transportation:</span> '+req.body.patientTransportation+'<br>'+
      '</td>'+
      '<td>'+
        '<span style="font-weight:bold;">D.O.B:</span> '+req.body.patientDob+'<br>'+
        '<span style="font-weight:bold;">Home Phone:</span> '+req.body.patientPhone+'<br>'+
        '<span style="font-weight:bold;">Cell Phone:</span> '+req.body.patientCell+'<br>'+
        '<span style="font-weight:bold;">Marital Status:</span> '+req.body.patientMarital_status+'<br>'+
      '</td>'+
    '</tr></table><div style="width:100%,padding:20px,height:5px"></div>'+
    '<table style="width:100%">'+
      '<tr><td><h2>Insurance Information</h2></td> <td><h2>Insured Contact Details</h2></td> <td><h2>Emergancy Contact</h2></td> </tr>'+
      '<tr><td><span style="font-weight:bold;">Employment Status:</span> '+req.body.ins_employment_status+'</td> <td><span style="font-weight:bold">Insured Name:</span> '+req.body.ins_lname+' '+req.body.ins_fname+'</td> <td><span style="font-weight:bold">Name:</span> '+req.body.emergency_name+'</td></tr>'+
      '<tr><td><span style="font-weight:bold;">Employer Name:</span> '+req.body.ins_employer_name+'</td> <td><span style="font-weight:bold">Home Address:</span> '+req.body.ins_address+', '+req.body.ins_city+' '+req.body.ins_state+' '+req.body.ins_zip+'</td> <td><span style="font-weight:bold">Number:</span> '+req.body.emergency_number+'</td></tr>'+
      '<tr><td><span style="font-weight:bold;">Reffered by:</span> '+req.body.ins_reffer+'</td> <td><span style="font-weight:bold">Phone:</span> '+req.body.ins_phone+'</td> <td></td></tr>'+
      '<tr><td></td> <td><span style="font-weight:bold;">Insured DOB:</span> '+req.body.ins_dob+'</td> <td></td></tr>'+
      '<tr><td></td> <td><span style="font-weight:bold;">Patient Relation:</span> '+req.body.patient_relation+'</td> <td></td></tr>'+
    '</table>'+
    '<table style="width:100%"><tr>'+
    '<h2>Insurance Info</h2>'+
        '<td><span style="font-weight:bold;">Insurance:</span> '+req.body.ins+'<br></td>'+
        '<td><span style="font-weight:bold;">Name:</span> '+req.body.patientName+'<br></td>'+
        '<td><span style="font-weight:bold;">Patient ID:</span> '+req.body.ins_dob+'<br></td>'+
        '<td><span style="font-weight:bold;">Co-pay:</span> '+req.body.copay+'<br></td>'+
        '<td><span style="font-weight:bold;">Deductable:</span> '+req.body.deductable+'<br></td>'+
    '</tr></table>'+
    '<br><br><br><br><span style="font-weight:bold;float:right;">Signature:____________  </span></div>';
    var options = { format: 'Letter' };
    let orgFileName = moment().unix()+".pdf";
    let filename = "C:\\Temp\\register\\pdf\\"+orgFileName;
    var fsCreate = require("fs");

    var dataWriteCreatPdf = '"C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe" "C:\\Temp\\register\\temp.html" "'+filename+'"';
    var printerCmd = 'START "Print Document" "C:\\Program Files (x86)\\Foxit Software\\Foxit Reader\\FoxitReader.exe" /t "'+filename+'" "NPI33B497 (HP LaserJet MFP M426fdw)" "NPI33B497 (HP LaserJet MFP M426fdw)" "WSD-c9a97b72-dfca-410f-a706-2e2b5cab075f"';
    fsCreate.writeFile("C:\\Temp\\register\\temp.html", html, function(err1, data1) {
      if (err1) {
            console.log(err1);
      }
      else{
        fsCreate.writeFile("C:\\Temp\\register\\createpdf.bat", dataWriteCreatPdf, function(err2, data2) {
          if (err2) {
                console.log(err2);
          }
          else{
            
            child_process.exec('C:\\Temp\\register\\createpdf.bat', function (error, stdout, stderr) {
                  //print file start
                    fsCreate.writeFile("C:\\Temp\\register\\printRegFile.bat", printerCmd, function(err1, data1) {
                      if (err1) {
                        console.log(err1);
                      }
                      else{
                        //console.log("Successfully Written to File.");
                        child_process.exec('C:\\Temp\\register\\printRegFile.bat', function (error, stdout, stderr) {
                          // res.status(200).json({
                          //   message: 'File print command success.'
                          // })
                        });
                      }
                    });
                  //print file end
            });
          }
        });
        
      }
    });
    // pdf.create(html, options).toFile(filename, function(err, res) {
    //   if (err){
    //     return console.log("result 309",err);}
    //   else{
    //     var fsCreate = require("fs");
    //     var dataWrite = 'START "Print Document" "C:\\Program Files (x86)\\Foxit Software\\Foxit Reader\\FoxitReader.exe" /t "'+filename+'" "Brother HL-5450DN series" "Brother HL-5450DN series" "USB001"';
    //     fsCreate.writeFile("printRegFile.bat", dataWrite, function(err1, data1) {
    //       if (err1) {
    //         console.log(err1);
    //       }
    //       else{
    //         //console.log("Successfully Written to File.");
    //         child_process.exec('C:/SpaApp/Register/printRegFile.bat', function (error, stdout, stderr) {
    //           // res.status(200).json({
    //           //   message: 'File print command success.'
    //           // })
    //         });
    //       }
    //     });
        
    //   }
    // });
    //End create pdf start
    //console.log(records);
    var json = JSON.stringify(records);
    fs.writeFile('C:/Temp/register/file.json', json, (err) => {
      if (!err) {
        child_process.exec('C:/Temp/register/runMSAccessNew.bat', function (error, stdout, stderr) {
          res.status(200).json({
            message: 'success'
          })
        });
      } else {
        res.status(500).json({
          message: 'error'
        })
      }
    });
  });

  //createArchieve
  app.post('/api/createArchieve', (req, res, next) => {
    // const records = {
    //   name: req.body.name,
    //   archiveNumber: req.body.archive
    // };
    // console.log(records);
    //var json = JSON.stringify(records);
    let csvData = req.body.name+","+req.body.archive;
    fs.writeFile('C:/Temp/register/archive.csv', csvData, (err) => {
      if (!err) {
        child_process.exec('C:/Temp/register/runMSExcelArch.bat', function (error, stdout, stderr) {
          res.status(200).json({
            message: 'Archive with excel update success'
          })
        });
      } else {
        res.status(500).json({
          message: 'error'
        })
      }
    });
  });
};